<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $image
 * @property string $text_preview
 * @property string $text
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $date_event
 */
class PageSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'author'], 'integer'],
            [['title','created_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'active' => 'Опубликовано',
            'created_at' => 'Дата создания',
            'author' => 'Автор',
        ];
    }

    public function search($params)
    {
        $query = Page::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'active', $this->active]);
        $query->andFilterWhere(['like', 'author', $this->author]);
        $query->andFilterWhere(['like', 'created_at', strtotime($this->created_at) ]);

        return $dataProvider;
    }


}
