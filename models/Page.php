<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use  yii\helpers\Html;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $image
 * @property string $text_preview
 * @property string $text
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $date_event
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active','date_event', 'author'], 'integer'],
            [['title', 'url', 'text_preview','text'], 'required'],
            ['url', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message'=>'Обязательны латинские буквы.'],
            [['title','url'], 'unique'],
            [['text'], 'string'],
            [ ['active'], 'default', 'value' => '0'],
            [ ['author'], 'default', 'value' => Yii::$app->user->id],
            [['title', 'url', 'image'], 'string', 'max' => 255],
            [['text_preview'], 'string', 'max' => 80],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'url' => 'Ссылка',
            'image' => 'Картинка',
            'text_preview' => 'Анонс',
            'text' => 'Текст',
            'active' => 'Опубликовано',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'date_event' => 'Дата события',
            'author' => 'Автор',
        ];
    }

    public static function dateConvertTimespamp($date_unix)
    {
        return date( Page::getDateFormat() ,$date_unix);
    }

    public static function getDateFormat()
    {
        return 'Y-m-d G:i:s';
    }

    public static function getDateFormatForWidjet()
    {
        return 'yyyy-mm-dd hh:ii:ss';
    }

    public function getFiles()
    {
        return $this->hasMany(File::className(), ['id_page' => 'id']);
    }

    public function getSimilarPages()
    {
        return $this->hasMany(SimilarPage::className(), ['id_page' => 'id']);
    }

    public function getSimilarPages2()
    {
        return $this->hasMany(SimilarPage::className(), ['id_similar_page' => 'id']);
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if (  $this->files )  foreach( $this->files as $obj )  $obj->delete();
        if (  $this->similarPages )  foreach( $this->similarPages  as $obj )  $obj->delete();
        if (  $this->similarPages2 )  foreach( $this->similarPages2  as $obj )  $obj->delete();

        // ...custom code here...
        return true;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }

    public function getUserName()
    {
        $user = $this->user;

        return $user  ? $user->name : '';
    }

    public static function getUsersList()
    {
        // Выбираем только те категории, у которых есть дочерние категории
        $users = User::find()
            ->select(['id', 'username'])
            ->all();

        return ArrayHelper::map($users, 'id', 'username');
    }


    public static function getStatusesArray()
    {
        return ['0'=>'Не опубликовано','1'=>'Опубликовано'];
    }

    public function getStatus()
    {
        if ( $this->active ) return Html::tag('div', 'Опубликовано', ['style'=>'color:#10b513']);
        else return Html::tag('div', 'Не опубликовано', ['style'=>'color:red']);
    }

    public function search($params)
    {
        $query = Page::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        //    ->andFilterWhere(['like', 'created_at', $this->created_at]);

        //$query->andFilterWhere(['title' => $this->title]);

        return $dataProvider;
    }


}
