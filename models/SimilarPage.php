<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "similar_page".
 *
 * @property integer $id
 * @property integer $id_page
 * @property integer $id_similar_page
 */
class SimilarPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'similar_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_page', 'id_similar_page'], 'required'],
            [['id_similar_page'], 'unique'],
            [['id_page', 'id_similar_page'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_page' => 'Id Page',
            'id_similar_page' => 'Id Similar Page',
        ];
    }
}
