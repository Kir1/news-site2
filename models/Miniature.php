<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $url
 * @property string $group
 */
class Miniature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'miniature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'url'], 'required'],
            [['image_id'], 'integer'],
            [['url','group'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Id Image',
            'url' => 'Url',
        ];
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        unlink( $this->url );

        // ...custom code here...
        return true;
    }

    public static function getFileUrl($url)
    {
        if ($url == '') return false;
        return Url::to($url, true);
    }
}
