<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $id_page
 * @property string $url
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_page', 'url','title'], 'required'],
            [['id_page'], 'integer'],
            [['img'], 'boolean'],
            [['url','group'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_page' => 'Id Page',
            'url' => 'Url',
        ];
    }


    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if ( File::checkUrl($this->url) && $this->miniatures )  foreach( $this->miniatures as $obj )  $obj->delete();

        unlink( $this->url );

        // ...custom code here...
        return true;
    }

    public function getMiniatures()
    {
        return $this->hasMany(Miniature::className(), ['image_id' => 'id']);
    }

    public static function checkUrl($path)
    {
        return ( exif_imagetype( $path) != false );
    }
}
