$( window ).load(function() {


		/////////
		$(".select2activ").select2();

		$( ".add_page" ).click(function() {
			var clone = $( ".similar_pages:first "  ).clone().appendTo(".similarPageContainer").select2(); //.similar_pages:first #w1
		});

		$( ".update_sim_page" ).click(function(ev) {
			ev.preventDefault();

			var parent = $(this).parent();

			$('#page-preloader').show();

			var arr = {
				'id_record': $(parent).attr('data-id-sim-page'),
				'id_similar_page': $(parent).find('.similar_pages').val(),
			};


			$.post( "/page/sim-page-update", arr)
				.done(function( data ) {
					$('#page-preloader').hide();
					$(parent).find('.messages').html( data );
				});
		});

		$( ".del_sim_page" ).click(function() {
			var parent = $(this).parent();
			$('#page-preloader').show();
			var arr = { 'id': $(parent).attr('data-id-sim-page') };
			$(parent).remove();
			$.post( "/page/sim-page-delete", arr)
				.done(function( data ) {
					$('#page-preloader').hide();
				});
		});

		////////////
		$( ".add_file" ).click(function() {
			$( ".fileLoadForm:first" ).clone().appendTo(".filesLoadContainer");
		});

		$( ".update-file" ).click(function(ev) {
			ev.preventDefault();

			var parent = $(this).parent();

			$('#page-preloader').show();

			var arr = {
				'id': $(parent).attr('data-id-file'),
				'title': $(parent).find('.title_file_upload').val(),
			};

			$.post( "/page/file-update", arr)
				.done(function( data ) {
					$('#page-preloader').hide();
					$(parent).find('.messages').html( data );
				});
		});

		$( ".del-file" ).click(function() {
		var parent = $(this).parent();
		$('#page-preloader').show();
		var arr = { 'id': $(parent).attr('data-id-file') };
		$(parent).remove();
		$.post( "/page/file-delete", arr)
			.done(function( data ) {
				$('#page-preloader').hide();
				console.log(data );
			});
		});

		////////////
		$('.colorbox').colorbox({rel:'gal'});
});