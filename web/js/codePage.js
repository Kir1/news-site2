$( window ).load(function() {
	if ( $('#image-gallery').length > 0 ) $('#image-gallery').lightSlider({
		gallery:true,
		item:1,
		thumbItem:9,
		slideMargin: 0,
		speed:500,
		auto:true,
		loop:true,
		onSliderLoad: function() {
			$('#image-gallery').removeClass('cS-hidden');
		}
	});

	////////////
	$('.colorbox').colorbox();
});