<?php

namespace app\controllers;

use app\models\Page;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
/*
if ($model->load(Yii::$app->request->post())) {
if ($model->validate()) {
    // form inputs are valid, do something here
return;
}
}
    */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => Page::find()->where(['active' => true]),
             'pagination' => [
                'pageSize' => $session->get('count_view'),
             ],
        ]);

        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }


    public static function getIdByUrl($url)
    {
         return Page::find()->where( ['url' =>
            str_replace("/news/", "", $url)
        ] )->select('id')->one()->id;
    }

    public function actionPage()
    {
        $id =  SiteController::getIdByUrl( Url::to('') );

        if (($model = Page::findOne($id)) !== null) {

            if ( $model->active == false
                && ParentController::checkAccAdminPanel() == false
            )
                throw new NotFoundHttpException('Страница не опубликована.');

            return $this->render('page', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('Такой страницы не существует.');
        };
    }

    public function actionCountView()
    {
        $arr = Yii::$app->request->post();
        $session = Yii::$app->session;
        $session->set('count_view',  $arr['count_view'] );
        return Yii::$app->response->redirect( Url::home(true) );
    }

    public function actionUsers()
    {
        return $this->render('@app/views/user/index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
