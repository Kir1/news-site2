<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Session;

class ParentController extends Controller
{
    public function beforeAction($action)
    {
        if (  ParentController::checkAccAdminPanel() ) {
            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/images/';
            Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/images/';
            return true;
        }
        else throw new ForbiddenHttpException('Access denied');
    }

    public static function checkAccAdminPanel()
    {
       return ( \Yii::$app->user->can('admin') || \Yii::$app->user->can('manager') );
    }

    public static function checkRoleAdmin()
    {
        return ( \Yii::$app->user->can('admin') );
    }

    public static function checkRoleManager()
    {
        return ( \Yii::$app->user->can('manager')   );
    }

    public static function checkManagerPage($id)
    {
        $checkPageAuthorManager = (new \yii\db\Query())
                ->select(['id'])
                ->from('page')
                ->where(['id' => $id])
                ->andWhere(['author' => Yii::$app->user->id ])
                ->one();

       return (
            ( ParentController::checkRoleManager() &&  $checkPageAuthorManager)
            || ParentController::checkRoleAdmin()
        );
    }

    public static function deniedManagerPage($id)
    {
        if ( ParentController::checkManagerPage($id) == false && ParentController::checkRoleAdmin()==false )
        throw new NotFoundHttpException('Вы можете работать только со своими страницами.');
        else return true;
    }
}
