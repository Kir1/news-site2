<?php

namespace app\controllers;

use app\models\File;
use app\models\Miniature;
use app\models\PageSearch;
use app\models\SimilarPage;
use Yii;
use app\models\Page;
use app\controllers\ParentController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use yii\data\ActiveDataProvider;

use yii\web\Controller;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends ParentController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionFileUpdate()
    {
        $model = new File();

        $post = \Yii::$app->request->post();

        $model = File::findOne( $post['id'] );
        $model->title = $post['title'];

        $arrMess = array();
        $class = 'alert-success';

        if ($model->validate()) {
            // all inputs are valid
            $model->save();
            $class = 'alert-success';
            $arrMess []= [0=>'Сохранено'];
        } else {
            // validation failed: $errors is an array containing error messages
            $class = 'alert-danger';
            $arrMess = $model->errors;
        }

        return $this->renderPartial('@app/views/site/message_bootsrtap', [
            'class' => $class,
            'messages' => $arrMess,
        ]);
    }

    public function actionSimPageUpdate()
    {
        $model = new SimilarPage();

        $post = \Yii::$app->request->post();

        $model = SimilarPage::findOne( $post['id_record'] );
        $model->id_similar_page = $post['id_similar_page'];

        $arrMess = array();
        $class = 'alert-success';

        if ($model->validate()) {
            // all inputs are valid
            $model->save();
            $class = 'alert-success';
            $arrMess []= [0=>'Сохранено'];
        } else {
            // validation failed: $errors is an array containing error messages
            $class = 'alert-danger';
            $arrMess = $model->errors;
        }

        return $this->renderPartial('@app/views/site/message_bootsrtap', [
            'class' => $class,
            'messages' => $arrMess,
        ]);
    }

    public function actionSimPageDelete()
    {
        SimilarPage::findOne( Yii::$app->request->post()['id'] )->delete();
    }

    public function actionFileDelete()
    {
        File::findOne( Yii::$app->request->post()['id'] )->delete();
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' =>  $searchModel,
        ]);
    }

    public static function prePageActions()
    {
        $arr = Yii::$app->request->post();
        if ( $arr  ) {
            $arr['Page']['date_event'] =  strtotime( $arr['Page']['date_event'] ) ;

            $vowels = array(" ", "/");
            $onlyconsonants = str_replace($vowels, "", $arr['Page']['url'] );
            $arr['Page']['url'] =   $onlyconsonants;
        }

        return  $arr;
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        $arr = PageController::prePageActions();

        if ($model->load( $arr ) && $model->save()) {
            $this->filesAndSimPages($model);
            return Yii::$app->response->redirect(Url::to([ 'news/'.$model->url ]));
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        ParentController::deniedManagerPage($id);

        $model = $this->findModel($id);

        $arr = PageController::prePageActions();

        if ($model->load( $arr ) && $model->save()) {
            $this->filesAndSimPages($model);
            return Yii::$app->response->redirect(Url::to([ 'news/'.$model->url ]));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        ParentController::deniedManagerPage($id);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function filesAndSimPages($model)
    {
        $arr = Yii::$app->request->post();

        //
        $files = UploadedFile::getInstancesByName('filesLoad');

        //
        $image = UploadedFile::getInstanceByName('imageLoad');
        if ( $image )  {
            $oldFile = File::findOne(
                [
                    'group' => 'main_image',
                    'id_page' =>  $model->id
                ]
            );
            if ( $oldFile)  $oldFile->delete();
            $image->name = $model->title.'.'.explode(".", $image->name)[1];

            $files []=  $image;
        }

        //print_r(  $files );
        if ($files && count($files)>0 ) foreach ($files as $num => $file) {
            $fileNameAndEx = explode(".", $file->name);
            $unicNameFile = File::find()->count().'.'.$fileNameAndEx[1]; //уник номер и разрешение файла
            $path = 'files/'.$unicNameFile;
            $file->saveAs( $path );

            $fileUrlBool = File::checkUrl($path) ;

            $object = new File();
            $object->id_page = $model->id;
            $object->url = $path;
            $object->title = (  isset( $arr['title_file_load'][$num]) && $arr['title_file_load'][$num] !=''  ? $arr['title_file_load'][$num] : $fileNameAndEx[0] );
            if ($file == $image) $object->group = 'main_image';
            else $object->group = 'file';
            $object->img = $fileUrlBool;
            $object->save();

            if ( $fileUrlBool ) {
                $unicNameFile = Miniature::find()->count().'.'.$fileNameAndEx[1];
                $pathMini = 'miniatures/edit/'.$unicNameFile;
                Image::getImagine()->open(  $path )->thumbnail(new Box(30, 30))->save( $pathMini , ['quality' => 90]);
                $objectNew = new Miniature();
                $objectNew->url = $pathMini;
                $objectNew->group = 'edit';
                $objectNew->image_id = $object->id;
                $objectNew->save();

                $unicNameFile = Miniature::find()->count().'.'.$fileNameAndEx[1];
                $pathMini = 'miniatures/carusel/'.$unicNameFile;
                Image::getImagine()->open(  $path )->thumbnail(new Box(100, 100))->save( $pathMini , ['quality' => 90]);
                $objectNew = new Miniature();
                $objectNew->url = $pathMini;
                $objectNew->group = 'carusel';
                $objectNew->image_id = $object->id;
                $objectNew->save();
            }
        }
        /////

        ///
        if ( count( $arr['similar_pages'] ) > 0 ) foreach ($arr['similar_pages'] as $id_page) {
            $similarPageHas = SimilarPage::find()
                ->where(['id_page' =>  $arr['Page']['id_page'],'id_similar_page' => $id_page])
                ->count();

            if ($similarPageHas==0) {
                $similarPage = new SimilarPage();
                $similarPage->id_page = $model->id;
                $similarPage->id_similar_page = $id_page;
                $similarPage->save();
            }
        }
        ////
    }
}
