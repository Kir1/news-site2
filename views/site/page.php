<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


$anonim = Yii::$app->user->isGuest;

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
    echo Breadcrumbs::widget([
        'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
        'links' => [
            $model->title,
        ],
    ]);
?>
<?php
if( \app\controllers\ParentController::checkManagerPage($model->id) ) {
?>
    <?= Html::a('Редактировать', ['page/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<?php
}
?>
<div class="row">
    <div class = "col-md-12 col-sm-12 col-xs-12" >
         <h1><?= Html::encode($this->title) ?></h1>
        <?php
        $dataEv = $model->date_event ;
        if ( $dataEv && $dataEv != 0) {

            $dataEv = \app\models\Page::dateConvertTimespamp(  $dataEv );
        ?>
          <h5><i>Дата события: <?= $dataEv?></i></h5>
        <?php
        }
        ?>
    </div>

    <div class = "col-md-12 col-sm-12 col-xs-12" >
        <div class = "row" >

            <?php
            $classBlock = 'col-md-12 col-sm-12 col-xs-12';
            $images = '';
            $files = '';

            if ($anonim == false) {
                $images = '';

                $images = \app\models\File::find()
                    ->where(['id_page' => $model->id])
                    ->andWhere(['img' => true])
                    ->andWhere(['!=','group' , 'main_image'])
                    ->select(['url', 'title', 'id'])
                    ->all();

                $image = \app\models\File::find()
                    ->where(['id_page' => $model->id])
                    ->andWhere(['group' => 'main_image'])
                    ->select(['url', 'title', 'id'])
                    ->one();

                $sliderCheck = ( $images &&  $image);

                if (  $sliderCheck ) array_unshift($images,  $image);

                $files = \app\models\File::find()
                    ->where(['id_page' => $model->id])
                    ->andWhere(['img' => false])
                    ->select(['url', 'title'])
                    ->all();

                $similarPages = \app\models\SimilarPage::find()
                    ->where(['id_page' => $model->id])
                    ->select(['id_similar_page'])
                    ->all();

                if ($image || $images || $files ) $classBlock = 'col-md-6 col-sm-12 col-xs-12';
            }
            ?>

             <div class = "<?= $classBlock ?>">

                 <?php
                 $this->registerCssFile('/lightslider-master/src/css/lightslider.css');

                 $this->registerJsFile('/lightslider-master/src/js/lightslider.js',
                     ['depends' => [\yii\web\JqueryAsset::className()]]);

                 $this->registerCssFile('/colorbox-master/example1/colorbox.css');

                 $this->registerJsFile('/colorbox-master/jquery.colorbox.js',
                     ['depends' => [\yii\web\JqueryAsset::className()]]);

                 $this->registerJsFile('/js/codePage.js',
                     ['depends' => [\yii\web\JqueryAsset::className()]]);
                 ?>


                 <?php
                 use yii\helpers\Url;

                 if( $sliderCheck ==false && $image) {
                     ?>
                     <a href = " <?= Url::home(true).$image->url ?>" class ="colorbox" title = '<?=  $model->title ?>' >
                         <?= Html::img( Url::home(true).$image->url, [ 'title' => $model->title,'alt' => $model->title,'class' => "image_slider" ] )  ?>
                     </a>

                     <?php
                 }
                 ?>

                <?php
                if(  $images ) {
                    ?>

                    <div class = "col-md-12 col-sm-12 col-xs-12" >
                        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                            <?php
                            foreach ($images as $image) {
                                $miniature = \app\models\Miniature::find()
                                ->where(['image_id' => $image->id])
                                ->andWhere( [ 'group' => 'carusel' ] )
                                ->one();
                                ?>
                                <li data-thumb="<?= Url::home(true).$miniature->url ?>">
                                    <a href = " <?= Url::home(true).$image->url ?>" class ="colorbox" title = ' <?=  $image->title ?>' >
                                        <?= Html::img( Url::home(true).$image->url, [ 'title' => $image->title,'alt' => $image->title,'class' => "image_slider" ] )  ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                    <?php
                }
                ?>


                <?php
                if(  $files ) {
                    ?>
                    <div class = "col-md-12 col-sm-12 col-xs-12" >
                        <h2>Файлы</h2>
                        <ul  class="">
                            <?php
                            foreach ( $files as $file) {
                                $miniature = \app\models\Miniature::find()
                                    ->where(['image_id' => $image->id])
                                    ->andWhere( [ 'group' => 'carusel' ] )
                                    ->one();
                                ?>
                                <li>
                                    <?= Html::a($file->title, [ $file->url], ['class' => 'profile-link', 'target'=>'_blank', 'title'=>$file->title ])   ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>


                 <?php
                 if(   $similarPages ) {
                     ?>
                     <div class = "col-md-12 col-sm-12 col-xs-12" >
                         <h2>Похожие страницы</h2>
                         <ul  class="">
                             <?php
                             foreach (   $similarPages as $similarPage ) {
                                 $similarPage = \app\models\Page::find()
                                     ->where(['id' =>  $similarPage->id_similar_page])
                                     ->andWhere( [ 'active' => true ] )
                                     ->select(['title','url'])
                                     ->one();
                                 ?>
                                 <li>
                                     <?= Html::a($similarPage->title, [ '/news/'.$similarPage->url], ['class' => 'profile-link', 'target'=>'_blank', 'title'=>$similarPage->title ])   ?>
                                 </li>
                                 <?php
                             }
                             ?>
                         </ul>
                     </div>
                     <?php
                 }
                 ?>

             </div>



            <div class = "<?= $classBlock ?>" >
                <?
                if ( $anonim ) {
                    echo $model->text_preview;
                    echo '<br>';
                    echo '<br>';
                    echo Html::tag('div', 'Авторизируйтесь, что бы увидеть полную версию страницы.', ['class' => ['alert', 'alert-warning']] );
                }
                else echo $model->text;
                ?>
            </div>

         </div>
    </div>
</div>


