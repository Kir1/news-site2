<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <label for="">Количество выводимых страниц: </label>
        <?
        $session = Yii::$app->session;

       // echo $session->get('count_view');
        //if ($session->has('count_view') == false ) $session->set('count_view', 1);

        $items = [
            '1' => '1',
            '10' => '10',
            '20'=>'20'
        ];
        $params = [
            'class' => 'count_view',
            'name' => 'count_view',
            'value' => $session->get('count_view'),
        ];
        echo Html::dropDownList('count_view', $session->get('count_view'), $items, $params);
        echo Html::button('Применить', ['class' => 'btn btn-primary btn-xs count_view_butt', 'style' =>'margin-left:10px']);
        ?>

        <?
        use yii\grid\GridView;
        use yii\helpers\Url;
        if ( $dataProvider ) {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Картинка',
                        'format' => 'raw',
                        'value' => function($data){
                            $main_page = \app\models\File::find()
                                ->where(['id_page' => $data->id])
                                ->andWhere(['=','group' , 'main_image'])
                                ->orderBy('id')
                                ->one();
                            if ( $main_page ) {
                                $miniature = \app\models\Miniature::find()
                                    ->where(['image_id' =>  $main_page->id])
                                    ->andWhere(['group' => 'edit'])
                                    ->one();

                                return Html::img(
                                    //Url::toRoute(
                                        $miniature->url
                                   // )
                                    ,[
                                    'alt'=>$main_page->title,
                                    'title'=>$main_page->title,
                                ]);
                            }
                        },
                    ],
                    [
                        'attribute' => 'title',
                        'label' => 'Заголовок',
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return ['class' => 'name'];
                        },
                        'content' => function ($data) {
                            return Html::a($data->title, Url::to('/news/'.$data->url, true));
                        }
                    ],
                    'text_preview',
                ],
            ]);
        }
        ?>

    </div>
</div>


<?php
$this->registerJsFile('/js/codeMainPage.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

?>