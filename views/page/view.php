<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use  app\controllers\ParentController;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = $model->title;

echo Breadcrumbs::widget([
        'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
        'links' => [
            [
                'label' => 'Pages',
                'url' => ['/page/index'],
                'template' => "<li>{link}</li>\n", // template for this link only
            ],
            $model->title,
        ],
    ]);
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['page/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['page/delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'url:url',
            'image',
            'text_preview',
            'text:ntext',
            'active',
            'created_at',
            'updated_at',
        ],
    ]);
    ?>

</div>
