<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = 'Update Page: ' . $model->title;

echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
        [
            'label' => 'Pages',
            'url' => ['/page/index'],
            'template' => "<li>{link}</li>\n", // template for this link only
        ],
        [
            'label' => $model->title,
            'url' => ['/news/'.$model->url],
            'template' => "<li>{link}</li>\n", // template for this link only
        ],
        'Edit',
    ],
]);
?>
<div class="page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
