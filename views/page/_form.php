<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/////////
//use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Page;
use kartik\datetime\DateTimePicker;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="page-form">

    <?= Html::hiddenInput('id_page', $model->id,  ['class' => 'id_page_hid']) ?>



    <?php $form = ActiveForm::begin( [ 'options'=>['enctype'=>'multipart/form-data'] ] ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?=
    '<label class="control-label" for="active">Опубликновано</label>'.
     CheckboxX::widget([
        'model' => $model,
        'attribute' => 'active',
         //'value'=>0,
        'pluginOptions' => [
            'threeState' => false
        ]
     ])
    ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?
    $objects = \dektrium\user\models\User::find()->all();

    if ($objects) {
        $items = ArrayHelper::map($objects, 'id', 'username');


        $params = [
            'prompt' => 'По-умолчанию автором будет создатель страницы.',
            'class' => 'select2activ'
        ];
        echo $form->field($model, 'author')->dropDownList($items,$params);
    }
    ?>

    <label for="">Картинка</label>
    <?
    $image =  \app\models\File::find()
        ->where(['id_page' => $model->id])
        ->andWhere(['group' => 'main_image'])
        ->one();
    ?>
    <?
    if (  $image ) {
        ?>
        <?
        $miniature = \app\models\Miniature::find()
            ->where(['image_id' => $image->id])
            ->andWhere(['group' => 'edit'])
            ->one();
        ?>
        <div>
            <a href = " <?= Url::home(true).$image->url ?>" class ="colorbox" title = ' <?=  $image->title ?>' >
                <?= Html::img( Url::home(true).'/'.$miniature->url, ['title' => $image->title,'alt' => $image->title]) ?>
            </a>
        </div>
        <?
        ?>
        <?
    }
    ?>
    </br>
    <div class = "">
            <?= Html::fileInput('imageLoad', '',  ['class' => 'file']) ?>
            <br>
    </div>
    <br>



    <label for="">Файлы</label>
    <?
     $files = \app\models\File::find()
    ->where(['id_page' => $model->id])
    ->andWhere(['!=','group' , 'main_image'])
    ->orderBy('id')
    ->all();
    if ($files) {
    ?>
        <?
        foreach( $files as $file) {
        ?>
        <div class = "file" data-id-file = '<?= $file->id ?>'>

            <?
            if ( $file->img  ) {
                $miniature = \app\models\Miniature::find()
                    ->where(['image_id' => $file->id])
                    ->andWhere(['group' => 'edit'])
                    ->one();
                ?>
                <a href = " <?= Url::home(true).$file->url ?>" class ="colorbox" title = ' <?=  $file->title ?>' >
                    <?= Html::img( Url::home(true).'/'.$miniature->url, ['title' => $file->title,'alt' => $file->title]) ?>
                </a>
                <?
            } else
                echo Html::a($file->title, [ $file->url], ['class' => 'profile-link', 'target'=>'_blank', 'title'=>$file->title ]);
            ?>

            <?= Html::textInput('title_file_upload', $file->title,  ['class' => 'title_file_upload', 'placeholder'=>'Имя файла']) ?>

            <?=  Html::tag('div', 'Обновить', ['class' => ['btn', 'btn-primary update-file']]); ?>
            <?=  Html::tag('div', 'Удалить', ['class' => ['btn', 'btn-danger del-file']]); ?>

            <div class = "messages"></div>

        </div>
         <br>
        <?
        }
        ?>
    <?
    }
    ?>

    <br>
    <br>
    <div class = "filesLoadContainer">
        <div class = "fileLoadForm">
            <?= Html::textInput('title_file_load[]', '',  ['class' => '', 'placeholder'=>'Имя файла']) ?>
            <?= Html::fileInput('filesLoad[]', '',  ['class' => 'file']) ?>
            <br>
        </div>
    </div>
    <?=  Html::tag('div', 'Ещё файл', ['class' => ['btn', 'btn-success add_file']]); ?>
    <br>
    <br>
    <br>


    <label for="">Похожие страницы</label>
    <?
    $objects = Page::find()
        ->where(['!=', 'id', $model->id])
        ->all();

    $items = ArrayHelper::map($objects, 'id', 'title');

    $similarPages = \app\models\SimilarPage::find()
        ->where(['id_page' => $model->id])
        ->all();
    if ( $similarPages )  {
        ?>
        <label for="">Похожие страницы</label>
        <br>
        <div class="similarSubPageContainer">
            <?
            foreach ($similarPages as $similarPage) {
                ?>
                <div class="similarSubPage" data-id-sim-page='<?= $similarPage->id ?>'>
                    <?
                    echo Html::dropDownList('similar_pages_sub[]', $similarPage->id_similar_page,
                        $items, ['class' => 'similar_pages select2activ']
                    );

                    echo Html::tag('div', 'Обновить', ['class' => ['btn', 'btn-primary update_sim_page']]);
                    echo Html::tag('div', 'Удалить', ['class' => ['btn', 'btn-danger del_sim_page']]);
                    ?>
                    <div class="messages"></div>
                    <br>
                </div>
                <?
            }
            ?>
        </div>
        <?
    }
    ?>

    <div class="similarPageContainer">
        <?
        if ($objects) {


            $params = [
                'prompt' => 'Выберите родительскую страницу',
                'class' => 'select2activ'
            ];

            echo 'Добавить<br>';
            echo '<br>';

            echo Html::dropDownList('similar_pages[]', null,
                $items, ['class' => 'similar_pages select2activ', 'prompt' => 'Выберите страницу']
            );
        }
        ?>
    </div>
    <br>
    <?= Html::tag('div', 'Ещё страницу', ['class' => ['btn', 'btn-success add_page']]); ?>
    <br>
    <br>


    <?= $form->field($model, 'text_preview')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ])
    ?>

    <label for="">Дата события</label>
    <?

    echo DateTimePicker::widget([
        'name' => 'Page[date_event]',
        'value' =>  Page::dateConvertTimespamp( $model->date_event ),
        'pluginOptions' => [
            'autoclose' => true,
            'format' =>  Page::getDateFormatForWidjet()
        ]
    ]);
    ?>

    <br>
    <br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerCssFile('/select2-master/dist/css/select2.css');

    $this->registerJsFile('/select2-master/dist/js/select2.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->registerJsFile('/js/livequery.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->registerCssFile('/colorbox-master/example1/colorbox.css');

    $this->registerJsFile('/colorbox-master/jquery.colorbox.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->registerJsFile('/js/code.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
