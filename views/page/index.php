<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Page;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;



?>

<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute'=>'author',
                'label'=>'Автор',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>function($data){
                    return $data->user->username;
                },
                'filter' => Page::getUsersList()
            ],
            [
                'filter' => Page::getStatusesArray(),
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                   return $model->getStatus();
                }
            ],
            [
                'attribute' => 'created_at',
                'format' =>  ['date', 'Y-MM-dd HH:mm:ss' ],
                'options' => ['width' => '200']
            ],
            [
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'view' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            '/news/'.$model->url);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
