<?php

use yii\db\Migration;

class m170724_153657_similar_page extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170724_153657_similar_new cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->createTable('similar_page', [
            'id' => $this->primaryKey(),
            'id_page' => $this->integer()->notNull(),
            'id_similar_page' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('similar_page');
    }
}
