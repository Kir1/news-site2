<?php

use yii\db\Migration;

class m170725_105647_user_add_column extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170725_105647_user_add_column cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('user', 'notification', $this->boolean()->defaultValue(true) );
    }


}
