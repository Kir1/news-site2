<?php

use yii\db\Migration;

class m170724_153810_notification extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170724_153810_notification cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'id_page' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'read' => $this->boolean()->defaultValue(false),
        ]);
    }

    public function down()
    {
        $this->dropTable('notification');
    }
}
