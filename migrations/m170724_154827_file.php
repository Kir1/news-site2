<?php

use yii\db\Migration;

class m170724_154827_file extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170724_154827_image_gallery_new cannot be reverted.\n";
        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'id_page' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
            'group' => $this->string(),
            'title' => $this->string(),
            'img' => $this->boolean()->defaultValue(false),
        ]);
    }

    public function down()
    {
        $this->dropTable('file');
    }

}
