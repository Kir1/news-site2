<?php

use yii\db\Migration;

class m170724_153624_page extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170724_153624_new cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'image' => $this->string(),
            'text_preview' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'active' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'date_event' => $this->integer(),
            'author' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('page');
    }
}
