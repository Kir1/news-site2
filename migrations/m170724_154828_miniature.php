<?php

use yii\db\Migration;

class m170724_154828_miniature extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170724_154828_miniature cannot be reverted.\n";
        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('miniature', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
            'group' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('miniature');
    }

}
